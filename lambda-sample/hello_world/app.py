import json
import logging
import os

# import requests

def lambda_handler(event, context):

    logger = initLog()

    logger.info("start aws lambda function sample")

    event_detail = json.dumps(event)
    context_detail = json.dumps(dir(context))

    print(event_detail)
    print(context_detail)

    body = json.dumps({
            "message": "Serverless Foundation - AWS Lambda",
            # "location": ip.text.replace("\n", "")
            "event_detail" : event_detail.replace("\n", ""),
            "context_detail" : context_detail.replace("\n", "")
        }, indent=4)

    logger.info("end aws lambda function sample")

    writeLog()

    return {
        "statusCode": 200,
        "body": body,
    }


def initLog():
    logger = logging.getLogger()

    if logger.handlers:
        for handler in logger.handlers:
            logger.removeHandler(handler)

    logging.basicConfig(format='%(asctime)s %(message)s',level=logging.INFO,filename='/tmp/logs.log', filemode="w")

    return logger

def writeLog():
    f = open("/tmp/logs.log")
    print("*"*20)
    for i in f.readlines():
        print(i)
    f.close
    print("*"*20)